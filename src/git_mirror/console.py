import click
import logging
import pygit2
import tempfile
import subprocess
import os
import configparser

from . import __version__


def debug_config(dbg_level):
    logging.basicConfig(level=dbg_level)

def git_clone_mirror(repo, dest):
    subprocess.call(['git', 'clone', '--mirror', repo, dest])

def _get_refs(repo, ref_t="refs/heads"):
    """
    Get a list of (heads/tags...) refs (e.g. 'refs/heads/*') retrieved by
    git-ls-remote. Return a list of refs with hashes and a list with refs names.

    Example:
    >>> mirror_head_refs[0]
    '0fcd9da17f3aaeb047be076fb2e7927dd9921c5a\trefs/heads/alsa-ucm-conf'
    >>> mirror_head_refs[0].split("\t")[1]
    'refs/heads/alsa-ucm-conf'
    """

    refs_l = []
    refs_name_l = []

    repo_refs = subprocess.run(['git', 'ls-remote', repo], capture_output=True)
    for r in repo_refs.stdout.splitlines():
        r = str(r.decode('ascii'))
        if ref_t in r:
            refs_l.append(r)
            refs_name_l.append(r.split("\t")[1])

    return refs_l, refs_name_l

def git_add_refs_mirror(upstream, mirror, dest):
    """
    Add the non-existent refs/*/<name> from the mirror repository to the
    upstream cloned (with --mirror option) repo folder.

    With this function we keep any deleted reference from upstream previously
    mirrored and also any other local ref created in the mirror repository.

    Warning! This does not prevent the following:
    -  If the mirror respository has the same ref name as in the remote upstream
       repository. Mirror ref will be overwritten.
    -  If the mirror respository ref diverges from the upstream. Mirror ref will
       be overwritten.
    """
    upstream_refs, upstream_refs_name = _get_refs(upstream, "refs/")
    mirror_refs, mirror_refs_name = _get_refs(mirror, "refs/")
    missing_refs = [x for x in mirror_refs_name if x not in upstream_refs_name]
    with open(dest + "/" + 'packed-refs', "a+") as f:
        for ref in mirror_refs:
            for r in missing_refs:
                if r in ref:
                    f.write(ref + "\n")
    f.close()

def git_push_mirror(repo):
    subprocess.call(['git', 'push', '--mirror', repo])

def git_mirror(upstream, mirror, del_refs):
    with tempfile.TemporaryDirectory() as tmpdirname:
        current_dir = os.getcwd()
        git_clone_mirror(upstream, tmpdirname)
        if del_refs:
            logging.warning("Mirror refs will be deleted.")
        else:
            git_add_refs_mirror(upstream, mirror, tmpdirname)
        os.chdir(tmpdirname)
        git_push_mirror(mirror)
        os.chdir(current_dir)

def config_init(inifile):
    config = configparser.ConfigParser()
    config.read(inifile)
    return config

def get_valid_sections(config):
    sections = []
    for s in config.sections():
        if "upstream" in config[s] and "mirror" in config[s]:
            sections.append(s)
    return sections

def mirror_sections(config, sections, del_refs):
    for i, s in enumerate(sections):
        upstream = config[s]["upstream"]
        mirror = config[s]["mirror"]
        logging.debug("upstream: {}".format(upstream))
        logging.debug("mirror: {}".format(mirror))
        git_mirror(upstream, mirror, del_refs)
        logging.debug("completed: {}/{} [{} %]".format(i+1, len(sections),
            (i+1)*100/len(sections)))

@click.version_option(version=__version__)
@click.group()
def main():
    """The git-mirror project.

    This gets the upstream repo and mirrors it.
    """
    click.echo("The git-mirror project!")
    debug_config(logging.DEBUG)

@main.command()
@click.option("--upstream", required=True, help="The upstream git repository")
@click.option("--mirror", required=True, help="The mirror git repository")
def mirror(mirror, upstream):
    logging.debug("mirror")

@main.command()
@click.option("--delete-mirror-refs", is_flag=True, required=False,
    help="Delete local refs in the mirrored respository.")
@click.option("--config-file", required=True, help="Use config file with a list of upstream" \
    "and mirror git repositories")
def config(config_file, delete_mirror_refs):
    """The config option."""
    config = config_init(config_file)
    s = get_valid_sections(config)
    mirror_sections(config, s, delete_mirror_refs)
