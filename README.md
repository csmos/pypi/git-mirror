# git-mirror

Simple git-mirror command line tool to mirror git repositories. By default, the
command, mirrors the upstream remote into the mirror remote, keeping all the refs
from the mirror if they are deleted/removed/renamed upstream. Notice when the
mirror ref has the same name than the upstream ref it will be overwritten.
Therefore, same behaviour will occur if any mirror ref (with the same ref
name in the upstream end) is updated.

## Option: `--delete-mirror-refs`

Option `--delete-mirror-refs` removes refs from the remote end (mirror).

Example:

```
$ poetry run git-mirror config --config-file mirror-qcom.conf
The git-mirror project!
DEBUG:root:['meta-qcom']
DEBUG:root:False
DEBUG:root:upstream: git://git.yoctoproject.org/meta-qcom.git
DEBUG:root:mirror: git@gitlab.com:csmos/mirrors/meta-qcom.git
Cloning into bare repository '/tmp/tmpe9q_l02_'...
remote: Enumerating objects: 4796, done.
remote: Counting objects: 100% (4796/4796), done.
remote: Compressing objects: 100% (2491/2491), done.
remote: Total 4796 (delta 2495), reused 3488 (delta 1871)
Receiving objects: 100% (4796/4796), 828.50 KiB | 1.14 MiB/s, done.
Resolving deltas: 100% (2495/2495), done.
Everything up-to-date
DEBUG:root:completed: 1/1 [100.0 %]
```

```
$ poetry run git-mirror config --config-file mirror-qcom.conf \
--delete-mirror-refs
The git-mirror project!
DEBUG:root:['meta-qcom']
DEBUG:root:True
DEBUG:root:upstream: git://git.yoctoproject.org/meta-qcom.git
DEBUG:root:mirror: git@gitlab.com:csmos/mirrors/meta-qcom.git
Cloning into bare repository '/tmp/tmpzb53vf9b'...
remote: Enumerating objects: 4796, done.
remote: Counting objects: 100% (4796/4796), done.
remote: Compressing objects: 100% (2491/2491), done.
remote: Total 4796 (delta 2495), reused 3488 (delta 1871)
Receiving objects: 100% (4796/4796), 828.50 KiB | 1.09 MiB/s, done.
Resolving deltas: 100% (2495/2495), done.
WARNING:root:Mirror refs will be deleted.
To gitlab.com:csmos/mirrors/meta-qcom.git
 - [deleted]         csmos
 - [deleted]         refs/merge-requests/1/head
DEBUG:root:completed: 1/1 [100.0 %]
```

## Future development

*  Replace `subprocess_call` when calling git commands with pygit2 python
   binding library.
*  Be able to create groups/subgroups and folders with GitLab and GitHub
   accounts if they do not exist.
